#!/bin/bash
DATE=$(date +%Y-%m-%d)
BRANCH="crontab/${DATE}"

git checkout -q -b "$BRANCH"

URLS=(
"http://lmtf.lilypie.com/fQqDp2.png"
"http://www.enceinte.com/reglette-349451.png"
"http://global.thebump.com/tickers/tt1c5186.aspx"
"http://global.thebump.com/tickers/tt1a87b2.aspx"
"http://tickers.babygaga.com/p/dev025pr___.png"
"http://www.pregnology.com/preggoticker2/777777/000000/My%20pregnancy/01/25/2016.png"
"http://www.neufmois.fr/les-tickers-suivi-de-grossesse/84110.png"
)

TARGETS=(
"lilypie"
"enceintepointcom"
"thebump"
"thebump-2"
"babygaga"
"pregnology"
"neufmois"
)

count=0
for URL in "${URLS[@]}"; do
  TARGET="images/${TARGETS[$count]}/${DATE}.png"
  #echo "$URL"
  #echo "$TARGET"
  if [ ! -f "$TARGET" -o ! -s "$TARGET" ]
    then
    /usr/local/bin/wget "$URL" -O "$TARGET"
    git add images
    git commit -m "${TARGET} created from ${URL}"
  fi
  #echo "-------"
  count=$(( count + 1 ))
done

git checkout -q master
git merge -q --no-ff --no-edit $BRANCH
git branch -q -d $BRANCH
git push -q origin

